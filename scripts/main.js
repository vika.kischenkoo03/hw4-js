/*1.Функції є основними "будівельними блоками" програми.Функції дозволяють повторно використовувати код:
 один раз визначити якийсь блок коду і потім використовуйте його скільки завгодно разів з будь-якого місця 
 скрипту. Також можна  використовувати один і той же код багато разів з різними аргументами, щоб отримувати різні результати.
  2. Передавати аргументи потрібно, щоб викликати функцію.
 Аргумент – це значення, передане в функцію під час її виклику (використовується під час виконання функції).
 Якщо викликати функцію без аргументів, тоді відповідні значення стануть undefined.
  3.Оператор return завершує виконання поточної функції і повертає її значення. Якщо не вказано значення, яке потрібно 
  повернути, повернеться undefined. Також функція негайно зупиняється у точці, де викликається return і не продовжиться далі.
 */

let numberFirst;
let numberSecond;
let operator;

do {
    numberFirst = prompt('Введіть перше число', [numberFirst]);
    result = parseInt(numberFirst);
} while (Number.isNaN(Number(result)));

do {
    numberSecond = prompt('Введіть друге число', [numberSecond]);
    result = parseInt(numberSecond);
} while (Number.isNaN(Number(result)));

operator = prompt('Введіть математичну операцію');

function answer() {
    switch (operator) {
        case '+':
            return +numberFirst + +numberSecond;
        case '-':
             return numberFirst - numberSecond;
        case '*':
            return numberFirst * numberSecond;
        case '/':
            return numberFirst / numberSecond;
        default:
            return "Помилка, ви не обрали математичну операцію"
    }
}

alert(answer());






